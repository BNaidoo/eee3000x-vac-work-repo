# imports
import sqlalchemy as sa
import pandas as pd

import dash
from dash import dcc, Output, Input, html, dash_table

import dash_bootstrap_components as dbc

import plotly.express as px

import plotly.graph_objects as go


# lack of import statements because Pycharm loads them automatically
# import statistics

# display full df for debugging
# pd.set_option('display.max_rows', None)
# this is for viewing the full lat and long coords for debugging
# pd.set_option('float_format', '{:f}'.format)

# ---------------------------------------------------------------------------

# BEFORE RUNNING THIS PROGRAM PLEASE CHECK THE FOLLOWING FUNCTIONS HAVE CORRECTLY ASSIGNED PARAMETERS
# BELOW IS THE LIST OF FUNCTIONS WITH DEFAULT PARAMETERS ASSIGNED

#  px.set_mapbox_access_token(
#         "[INSERT ACCESS TOKEN HERE]")

# engine = sa.create_engine('SQL DATABASE ADDRESS'
#                               .format(user='USER', password='PASSWORD', server='SERVER', database='DATABASE'),
#                               echo=False, future=True)

#     with engine.connect() as con:
#         df = pd.read_sql('DATABASE NAME', con)

# --------------------------------------------------------------------------

def serve_layout():
    global app, df

    px.set_mapbox_access_token(
        "[INSERT ACCESS TOKEN HERE]")
    # sql alcemy engine creation
    #
    # echo: True/False - engine logs all sql it emits to python logger
    # future: True - allows for use of sql alchemies v2.0 style
    # (more for example convenience)
    #

    engine = sa.create_engine('SQL DATABASE ADDRESS'
                              .format(user='USER', password='PASSWORD', server='SERVER', database='DATABASE'),
                              echo=False, future=True)

    #
    # re-run this block to read from the database
    # with statement used to manage context of Connection obj returned from .connect method
    # , ensures a Connection.close() method is invoked at the end of this block
    #
    with engine.connect() as con:
        df = pd.read_sql('DATABASE NAME', con)

    # df #check table then drop useless columns

    '''
    sorts values from latest to earliest 
    then removes duplicates 
    '''
    df = df.sort_values(by='datetime', ascending=False)
    df = df.drop_duplicates(subset='place_id', keep='first')

    cols = ['outside_pic', 'index', 'google_map', 'AutoInc', 'email', 'contact_no', 'street_no',
            'street_name', 'district', 'municipality', 'city', 'own_man_first_name',
            'own_man_surname', 'datetime', 'gen_notes']
    df = df.drop(columns=(cols))
    # df_GP = df[df['province'] == 'Gauteng']
    # df_KZN = df[df['province'] == 'KwaZulu-Natal']
    # ---------------------------------------------------------------------------
    '''
    there is an error of magnitude in the 
    lat and long for Yummy Roots Food that 
    I am manually fixing in this block 
    
    row_index = df_GP.index[df_GP['place_id'] == '3657c362-ffa9-47b0-b9f0-fa26c87eea33']
    #print(row_index)
    df_GP = df_GP.drop(row_index)
    '''

    '''
    A more robust solution would be to drop rows which have lats and longs 
    beyond limits:
    -90 to 90 for lat
    -180 to 180 for long 
    
    
    the more robust solution:
    '''
    df = df.drop(df[df.lati < -90].index)
    df = df.drop(df[df.lati > 90].index)
    df = df.drop(df[df.long < -180].index)
    df = df.drop(df[df.lati > 180].index)

    # test = df[df.province == 'Gauteng']
    # print(test.lati.mean())

    # ---------------------------------------------------------------------------

    '''
    a small block to calculate avg lat and long 
    to use in the center parameter
    of the mapbox figures (gives better centering)
    '''
    # first delete all test points where rep == 'admin'
    df = df.drop(df[df.rep == 'admin'].index)

    # an array of provinces present in the df/ sql db
    # provinces = df['province'].unique()

    # editing the pic link col in the df:
    df['outside_pic link'] = '[Click]' + '(' + df['outside_pic link'] + ')'
    # ---------------------------------------------------------------------------

    app = dash.Dash(__name__, external_stylesheets=[dbc.themes.LUX])

    # ---------------------------------------------------------------------------
    '''
    app layout
    menus have been programmed such that addition of new points in db and df
    should auto populate
    '''

    app.layout = dbc.Container([

        dbc.Row([
            dbc.Col(html.Div('Supply Pal Logo Here'), width=2),
            dbc.Col(html.H1('Supply Pal General Business Survey'), width={'size': 6, 'offset': 2}),
            dbc.Col(html.Div('Supply Pal Analytics Copyright Notice Here'), width={'size': 2, 'offset': 0})
        ], justify='center'),

        dbc.Row([
            dbc.Col(html.Br()),
            dbc.Col(html.Br())
        ]),

        dbc.Row([
            dbc.Col(html.H2('Geographical Information Display'), width='auto')
        ], justify='center'),

        dbc.Row([
            dbc.Col(html.Br())
        ]),

        dbc.Row([
            dbc.Col(html.Div('Select a province to view statistics for:'), width={'size': 'auto', 'offset': 0}),
            dbc.Col(dcc.Dropdown(id='prov_dpdn', multi=False, clearable=False, searchable=True,
                                 placeholder='Select a province to view statistics for',
                                 options=[{'label': x, 'value': x}
                                          for x in sorted(df['province'].unique())],

                                 # [
                                 # {'label':'Eastern Cape','value':'EC'},
                                 # {'label':'Free State','value':'FS'},
                                 # {'label':'Gauteng','value':'GP'},
                                 # {'label':'KwaZulu-Natal','value':'KZN'},
                                 # {'label':'Limpopo','value':'LM'},
                                 # {'label':'Mpumalanga','value':'MP'},
                                 # {'label':'Northern Cape','value':'NC'},
                                 # {'label':'North West','value':'NW'},
                                 # {'label':'Western Cape','value':'WC'}],
                                 value='KwaZulu-Natal'
                                 ), width=4)

        ], justify='center'),

        # dbc.Row([
        #   dbc.Col(html.Div("Place type/s:"), width = 'auto'),
        #  dbc.Col(dcc.Dropdown(id = 'placetype_dpdn', multi = True, clearable = True,
        #                      placeholder = 'Select a place type/s to filter displayed map data by',
        #                    options = [{'label': x, 'value': x}
        #                             for x in sorted(df['place_type'].unique())]),
        #         width = {'size': 6, 'offset': 0})

        # ]),

        dbc.Row([
            dbc.Col(html.Div("Customer access type/s:"), width={'size': 2, 'offset': 2}),
            dbc.Col(dcc.Checklist(id='access_check',
                                  options=[],
                                  value=[],
                                  # options=[{'label': x, 'value': x}
                                  #        for x in sorted(df['customer_access'].unique())],
                                  # value=df['customer_access'].unique()
                                  inline=True, inputStyle={"margin-left": "5px"}),
                    width={'size': 7, 'offset': 0})

        ], justify='center'),

        dbc.Row([
            dbc.Col(html.Div("Premises type/s:"), width={'size': 2, 'offset': 2}),
            dbc.Col(dcc.Checklist(id='premises_check',
                                  # options=[{'label': x, 'value': x}
                                  #          for x in sorted(df['premises_type'].unique())],
                                  # value=df['premises_type'].unique()
                                  inline=True, inputStyle={"margin-left": "5px"}),
                    width={'size': 7, 'offset': 0})

        ], justify='center'),

        dbc.Row([
            dbc.Col(html.Br())
        ]),

        dbc.Row([
            dbc.Col(html.Div('Select a map view:'), width={'size': 'auto', 'offset': 1}),
            dbc.Col(dcc.Dropdown(id='map_dpdn', multi=False, clearable=False, searchable=True,
                                 placeholder='Select a map view',
                                 options=[
                                     # {'label': 'Default', 'value': 'open-street-map'},
                                     {'label': 'Street View', 'value': 'streets'},
                                     {'label': 'Street View with Satellite', 'value': 'satellite-streets'}],
                                 value='streets',
                                 ), width={'size': 2})

        ], justify='start'),

        dbc.Row([
            dbc.Col(dcc.Graph(id='geo_fig', figure={}, config={'displayModeBar': False}), width=12)

        ], justify='center'),

        dbc.Row([
            dbc.Col(html.H2('General Statistical Overview for selected province'), width='auto')

        ], justify='center'),

        dbc.Row([
            dbc.Col(html.H4(id='counter_1', children=[
                'Number of logged businesses in selected province:',
                html.Span(id='total', children='')]),
                    width='auto')
        ], justify='center'),

        dbc.Row([
            dbc.Col(dcc.Graph(id='placetype_fig', figure={}, config={'displayModeBar': False}), width=4),
            dbc.Col(dcc.Graph(id='premises_fig', figure={}, config={'displayModeBar': False}), width=4),
            dbc.Col(dcc.Graph(id='access_fig', figure={}, config={'displayModeBar': False}), width=4)
        ], justify='start'),

        #  dbc.Row([
        #     dbc.Col(dcc.Graph(id='premises_fig', figure={}), width='auto')
        # ], justify='start'),

        # dbc.Row([
        #     dbc.Col(dcc.Graph(id='access_fig', figure={}), width=4)
        # ]),

        dbc.Row([
            dbc.Col(html.H2('Detailed  Overview for Selected Province'), width='auto')
        ], justify='center'),

        dbc.Row([
            dbc.Col(dash_table.DataTable(
                columns=[{'name': 'Place Name', 'id': 'place_name', 'deletable': False, 'hideable': True,
                          'presentation': 'markdown', 'selectable': False},
                         {'name': 'Place Type', 'id': 'place_type', 'deletable': False, 'hideable': True,
                          'presentation': 'markdown'},
                         {'name': 'Province', 'id': 'province', 'deletable': False, 'hideable': True,
                          'presentation': 'markdown'},
                         {'name': 'Suburb', 'id': 'suburb', 'deletable': False, 'hideable': True,
                          'presentation': 'markdown'},
                         {'name': 'Customer Access Type', 'id': 'customer_access', 'deletable': False,
                          'hideable': True, 'presentation': 'markdown'},
                         {'name': 'Premises Type', 'id': 'premises_type', 'deletable': False, 'hideable': True,
                          'presentation': 'markdown'},
                         {'name': 'Picture Link', 'id': 'outside_pic link', 'deletable': False, 'hideable': True,
                          'presentation': 'markdown'}
                         ],
                data=df.to_dict('records'),
                id='table', editable=False, filter_action='native', sort_action='native', sort_mode='multi',
                page_current=0, page_action='native', style_cell_conditional=[{'textAlign': 'left'}],
                export_columns='visible', export_format='csv',
                row_selectable=False,
                # page_size=10,
                fixed_columns={'headers': True, 'data': 1},
                fixed_rows={'headers': True, 'data': 0},
                style_cell={'minWidth': '300px', ',maxWidth': '300px', 'width': '300px',
                            'whitespace': 'normal'},
                # style_cell={'minWidth': '250px', ',maxWidth': '250px', 'width': '250px'},
                style_table={'overflowX': 'scrollng', 'minWidth': '100%', 'minHeight': '100%'}
            ), width=12, sm=12, md=12, lg=12)
        ], justify='start'),

    ], fluid=True)
    return app


app = serve_layout()

# ---------------------------------------------------------------------------
'''
callbacks
'''


# callbacks for pie charts
@app.callback(
    Output(component_id='total', component_property='children'),
    Output(component_id='placetype_fig', component_property='figure'),
    Output(component_id='premises_fig', component_property='figure'),
    Output(component_id='access_fig', component_property='figure'),
    Input(component_id='prov_dpdn', component_property='value')
)
def update_graphs(option_slctd):
    dff = df.copy()
    dff = dff[dff['province'] == option_slctd]

    total = len(dff)

    labels = dff.place_type.value_counts().index
    values = dff.place_type.value_counts().values
    place_pie = go.Figure(data=[go.Pie(labels=labels, values=values, title='Place Types',
                                       hovertemplate="%{label} <br>Count: %{value} </br>Percentage: %{percent} "
                                                     "<extra></extra>")])

    place_pie.update_traces(title_font_size=20)
    place_pie.update_traces(title_position='top left')
    place_pie.update_layout(showlegend=False)
    place_pie.update_traces(textposition='inside')
    # place_pie.update_traces(hoverinfo='percent', selector=dict(type='pie'))
    place_pie.update_layout(uniformtext_minsize=12, uniformtext_mode='hide')

    labels = dff.premises_type.value_counts().index
    values = dff.premises_type.value_counts().values
    prem_pie = go.Figure(data=[go.Pie(labels=labels, values=values, title='Premises Types',
                                      hovertemplate="%{label} <br>Count: %{value} </br>Percentage: %{percent} "
                                                    "<extra></extra>")])

    prem_pie.update_traces(title_font_size=20)
    prem_pie.update_traces(title_position='top left')
    prem_pie.update_traces(textposition='inside')
    prem_pie.update_layout(showlegend=False)

    labels = dff.customer_access.value_counts().index
    values = dff.customer_access.value_counts().values
    acc_pie = go.Figure(data=[go.Pie(labels=labels, values=values, title='Customer Access Types',
                                     hovertemplate="%{label} <br>Count: %{value} </br>Percentage: %{percent}"
                                                   "<extra></extra>")])

    acc_pie.update_traces(title_font_size=20)
    acc_pie.update_traces(title_position='top left')
    acc_pie.update_traces(textposition='inside')
    acc_pie.update_layout(showlegend=False)

    return total, place_pie, prem_pie, acc_pie,


# callbacks for populating customer access checkboxes based of selected province from prov_dpdn
@app.callback(
    Output(component_id='access_check', component_property='options'),
    Output(component_id='access_check', component_property='value'),
    Input(component_id='prov_dpdn', component_property='value')
)
def update_acc_check(optn_slctd):
    dff = df.copy()
    dff = dff[dff['province'] == optn_slctd]
    options = sorted(dff.customer_access.unique())
    values = sorted(dff.customer_access.unique())
    return options, values


# callbacks for populating premises checkboxes based of selected province and access types
@app.callback(
    Output(component_id='premises_check', component_property='options'),
    Output(component_id='premises_check', component_property='value'),
    Input(component_id='prov_dpdn', component_property='value'),
    Input(component_id='access_check', component_property='value')

)
def update_prem_check(prov_sel, acc_sels):
    if len(acc_sels) == 0:
        return dash.no_update
    else:
        dff = df.copy()
        dff = dff[(dff.province == prov_sel) & (dff.customer_access.isin(acc_sels))]
        options = sorted(dff.premises_type.unique())
        values = sorted(dff.premises_type.unique())
        return options, values


# mapbox figure and datatable callbacks:
@app.callback(
    Output(component_id='geo_fig', component_property='figure'),
    # Output(component_id='table', component_property='columns'),
    Output(component_id='table', component_property='data'),
    Input(component_id='prov_dpdn', component_property='value'),
    Input(component_id='access_check', component_property='value'),
    Input(component_id='premises_check', component_property='value'),
    Input(component_id='map_dpdn', component_property='value')
)
def update_fig(prov_sel, acc_sels, prem_sels, map_dpdn):
    if len(acc_sels) == 0 or len(prem_sels) == 0:
        return dash.no_update
    else:
        dff = df.copy()
        dff = dff[(dff.province == prov_sel) & (dff.customer_access.isin(acc_sels))
                  & (dff.premises_type.isin(prem_sels))]

        dff['lati'] = dff.lati
        dff['long'] = dff.long
        fig_map = px.scatter_mapbox(
            data_frame=dff,
            lat=dff['lati'],
            lon=dff['long'],
            center={'lat': dff.lati.mean(), 'lon': dff.long.mean()},
            zoom=10,
            hover_name='place_name',
            hover_data={'place_type': True, 'customer_access': True, 'premises_type': True, 'lati': False,
                        'long': False},
            mapbox_style=map_dpdn,
            color='place_type',
            labels={'place_type': 'Place Type', 'customer_access': 'Customer Access Type',
                    'premises_type': 'Premises Type'}

        )

        drop_cols = ['rep', 'lati', 'long', 'nationality', 'place_id']
        # keep_cols = ['Place Name', 'Place Type', 'Customer Access Type', 'Premises Type', 'Picture Link']
        dff = dff.drop(columns=drop_cols)
        # dff['outside_pic link'] = '[Click]' + '(' + dff['outside_pic link'] + ')'
        # columns = [{'name': 'Place Name', 'id': 'place_name', 'deletable': False, 'hideable': True, 'selectable': True},
        #            {'name': 'Place Type', 'id': 'place_type', 'deletable': False, 'hideable': True},
        #            {'name': 'Customer Access Type', 'id': 'customer_access', 'deletable': False, 'hideable': True},
        #            {'name': 'Premises Type', 'id': 'premises_type', 'deletable': False, 'hideable': True},
        #            {'name': 'Picture Link', 'id': 'outside_pic link', 'deletable': False, 'hideable': True}
        #            ]

        # if i == 'place_id' or i == 'outside_pic link'
        # else {'name': i, 'id': i, 'deletable': False, 'hideable': False}
        # for i in keep_cols]
        data = dff.to_dict('records')

        return fig_map, data


if __name__ == '__main__':
    app.run_server(debug=True)

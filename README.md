# EEE3000X Vac Work Repo 

## Description
This repo is meant to simply serve as a catalogue of the vac work that I undertook in order to complete my EEE3000X course module. It contains the code used to build an analytical dashboard using Dash in Python as well as the report I generated as part of my work. The code has been scrubbed of any sensitive information such as login details and database addresses, it is essentially a non-functional skeleton in its current form. 

Please also note that the hyperlinks on the corporate report are broken. This is because there was no need to continuously host the application after my work period had concluded. 

## License
This work and repo are purely for academic purposes therefore, no license is granted for commercial use.


